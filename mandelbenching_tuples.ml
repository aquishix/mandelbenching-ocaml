let ripOffThe6th (a, b, c, d, e, f) = f;;

let rec mandelbrot (sextuple : float * float * float * float * int * string) : (float * float * float * float * int * string) =   
  let (cre, cim, zre, zim, its, retChar) = sextuple in
  let (magn: float) = (zre *. zre +. zim *. zim) in
  if magn >= 4.0 && its >= 32 then (cre, cim, zre, zim, its, "·") else
    if magn >= 4.0 && its >= 16 then (cre, cim, zre, zim, its, "*") else
       if magn >= 4.0 then (cre, cim, zre, zim, its, "#") else 
         if its >= 1000000 then (cre, cim, zre, zim, its, " ") else
           mandelbrot (cre, cim, zre *. zre -. zim *. zim +. cre, 2.0 *. zre *. zim +. cim, its + 1, "-");;

for y = 19 downto -20 do
  let cim = (float y) /. 20.0 in
  for x = -40 to 39 do
    let cre = (float x) /. 40.0 -. 0.5 in
    print_string (ripOffThe6th (mandelbrot (cre, cim, 0.0, 0.0, 0,
    "this_doesn't_matter")));
    flush stdout;
  done;
  print_string "\n" 
done;;

